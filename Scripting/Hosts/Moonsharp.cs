﻿using MoonSharp.Interpreter;
using Scripting.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Scripting.Hosts
{
    class MoonSharpHost
    {
        private Script script = new Script();
        private ICollection<Entity> entities;

        public MoonSharpHost(ICollection<Entity> entities, TextWriter textWriter = null)
        {
            if (null != textWriter)
            {
                script.Options.DebugPrint = s => textWriter.Write(s);
            }
            this.entities = entities;
        }

        public string ExecuteCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                return null;
            }
            UserData.RegisterAssembly();
            script.Globals["entities"]= entities;

            DynValue result;
            try
            {
                result = script.DoString(code);
                return result.ToPrintString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }  
    }
}

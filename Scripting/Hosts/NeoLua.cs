﻿using Neo.IronLua;
using Scripting.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Scripting.Hosts
{
    class NeoLuaHost
    {
        private readonly Lua scriptEngine = new Lua();
        private dynamic environment;
        private TextWriter textWriter;
        private ICollection<Entity> entities;

        public NeoLuaHost(ICollection<Entity> entities, TextWriter textWriter = null)
        {
            environment = scriptEngine.CreateEnvironment<LuaGlobal>();

            if (null != textWriter)
            {
                this.textWriter = textWriter;
                environment.print = new Action<object[]>(Print);
            }
            environment.entities = entities;
            this.entities = entities;
        }

        public string ExecuteCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                return null;
            }

            dynamic result;
            try
            {
                LuaChunk chunk = scriptEngine.CompileChunk(code,
                    "test.lua",
                    new LuaCompileOptions() { DebugEngine = LuaStackTraceDebugger.Default }
                ); // compile the script with debug informations, that is needed for a complete stack trace
                result = environment.dochunk(chunk); // execute the chunk

                return Convert.ToString(result) ?? string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private void Print(object[] texts)
        {
            foreach (object o in texts)
                textWriter.Write(Convert.ToString(o));
        }
    }
}

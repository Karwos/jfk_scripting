﻿namespace Scripting
{
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Input;

    using Scripting.Hosts;
    using Scripting.Model;
    using Scripting.ViewModel;

    using Microsoft.Win32;

#pragma warning disable CC0091 // Use static method
    public partial class MainWindow
    {
        private static readonly EmailAddressAttribute EmailValidator = new EmailAddressAttribute();
        private readonly StringBuilder stringBuilder = new StringBuilder();
        private readonly StringBuilder stringBuilder2 = new StringBuilder();
        private readonly NeoLuaHost neoLuaHost;
        private readonly MoonSharpHost moonSharpHost;

        /// <inheritdoc />
        public MainWindow()
        {
            this.InitializeComponent();
            var outputWriter = new StringWriter(this.stringBuilder);
            this.neoLuaHost = new NeoLuaHost(this.ModelView.Entities, outputWriter);
            var outputWriter2 = new StringWriter(this.stringBuilder2);
            this.moonSharpHost = new MoonSharpHost(this.ModelView.Entities, outputWriter2);

            this.Closing += (sender, e) => outputWriter.Dispose();
            this.Closing += (sender, e) => outputWriter2.Dispose();
        }

        private MainWindowViewModel ModelView => (MainWindowViewModel)this.DataContext;
         
        private void AlwaysCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ExecutedExit(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void ExecutedNew(object sender, ExecutedRoutedEventArgs e)
        {
            Entity en = this.GetEntity();
            if (Validate(en))
                this.ModelView.Entities.Add(this.GetEntity());
            else MessageBox.Show("Zakres wartosci ceny to <0,0 - 1000,0> a kategorii PEGI <3 - 18>");
        }

        private void ExecutedSave(object sender, ExecutedRoutedEventArgs e)
        {
            if (!(this.SelectedEntityGrid.DataContext is Entity selectedEntity))
            {
                return;
            }

            var updatedEntity = this.GetEntity();
            if (Validate(updatedEntity))
            {
                selectedEntity.Name = updatedEntity.Name;
                selectedEntity.Genre = updatedEntity.Genre;
                selectedEntity.Producer = updatedEntity.Producer;
                selectedEntity.Price = updatedEntity.Price;
                selectedEntity.Pegi = updatedEntity.Pegi;
                selectedEntity.YearofPremiere = updatedEntity.YearofPremiere;
            }
            else MessageBox.Show("Zakres wartosci ceny to <0,0 - 1000,0> a kategorii PEGI <3 - 18>");

        }

        private void ExecutedDelete(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedIndex = this.DataGrid.SelectedIndex;
            if (selectedIndex > -1)
            {
                this.ModelView.Entities.RemoveAt(selectedIndex);
            }
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrWhiteSpace(this.NameTextBox.Text) &&
                !string.IsNullOrWhiteSpace(this.GenreTextBox.Text) &&
                !string.IsNullOrWhiteSpace(this.ProducerTextBox.Text) &&
                double.TryParse(this.PriceTextBox.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out _) &&
                !string.IsNullOrWhiteSpace(this.PegiTextBox.Text) &&
                !string.IsNullOrWhiteSpace(this.YearofPremiereTextBox.Text);
        }

        private void CanExecuteSave(object sender, CanExecuteRoutedEventArgs e)
        {
            if (!(this.SelectedEntityGrid.DataContext is Entity))
            {
                e.CanExecute = false;
            }
            else
            {
                this.CanExecute(sender, e);
            }
        }

        private void CanExecuteDelete(object sender, CanExecuteRoutedEventArgs e)
        {
            var selectedIndex = this.DataGrid.SelectedIndex;

            if (selectedIndex < 0)
            {
                e.CanExecute = false;
            }
            else
            {
                this.CanExecute(sender, e);
            }
        }

        private void ExecutedNeoLua(object sender, ExecutedRoutedEventArgs e)
        {
            var resultNeoLua = this.neoLuaHost.ExecuteCode(e.Parameter as string);

            if (!string.IsNullOrEmpty(resultNeoLua))
                {
                    this.ModelView.NeoLuaText = resultNeoLua;
                }

                this.ModelView.NeoLuaOutput = this.stringBuilder.ToString();
            this.stringBuilder.Clear();
        }

        private void ExecutedMoonSharp(object sender, ExecutedRoutedEventArgs e)
        {
            var resultMoonSharp = this.moonSharpHost.ExecuteCode(e.Parameter as string);

            if (!string.IsNullOrEmpty(resultMoonSharp))
            {
                this.ModelView.MoonSharpText = resultMoonSharp;
            }

            this.ModelView.MoonSharpOutput = this.stringBuilder2.ToString();
            this.stringBuilder2.Clear();
        }

        private void CanExecuteNeoLua(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrWhiteSpace(this.NeoLuaBox.Text);
        }

        private void CanExecuteMoonSharp(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrWhiteSpace(this.MoonSharpBox.Text);
        }

        private Entity GetEntity()
        {
           
             return new Entity(
                this.NameTextBox.Text,
                this.GenreTextBox.Text,
                this.ProducerTextBox.Text,
                double.Parse(this.PriceTextBox.Text,NumberStyles.Currency,CultureInfo.InvariantCulture),
                int.Parse(this.PegiTextBox.Text),
                int.Parse(this.YearofPremiereTextBox.Text));

        }

        private bool Validate (Entity en)
        {
            double newprice = en.Price;
            int newpegi = en.Pegi;
            if (newprice > 0.0 && newprice <= 1000.0 && newpegi >= 3 && newpegi <= 18) return true;
            else return false;
        }

        private void SaveFile_Handler(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog { AddExtension = true, DefaultExt = "csv", Filter = ".csv|*.csv" };
            if (true == saveFileDialog.ShowDialog())
            {
                File.WriteAllText(saveFileDialog.FileName, this.ModelView.GetSerializedEntities());
            }
        }

        private void OpenFile_Handler(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                AddExtension = true,
                DefaultExt = "csv",
                Filter = ".csv|*.csv"
            };
            if (true == openFileDialog.ShowDialog())
            {
                this.ModelView.LoadSerializedEntities(File.ReadAllText(openFileDialog.FileName));
            }
        }
    }
#pragma warning restore CC0091 // Use static method
}

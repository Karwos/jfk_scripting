﻿namespace Scripting.Model
{
    using MoonSharp.Interpreter;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.CompilerServices;

    // "Steam Market" Entity
    [MoonSharpUserData]
    public class Entity : INotifyPropertyChanged
    { 
        private string name, genre, producer;
        private int pegi, yearofPremiere;
        private double price;

        public Entity()
        {

        }

        public Entity(string name, string genre, string producer, double price, int pegi, int yearofPremiere)
        {
            this.name = name;
            this.genre = genre;
            this.producer = producer;
            this.price = price;
            this.pegi = pegi;
            this.yearofPremiere = yearofPremiere;
        }
 

        public event PropertyChangedEventHandler PropertyChanged;

        [Required]
        public string Name
        {
            get => this.name;
            set
            {
                if (SetField(ref this.name, value))
                {
                    this.RaisePropertyChanged();
                }
            }
        }

        [Required]
        public string Genre
        {
            get => this.genre;
            set
            {
                if (SetField(ref this.genre, value))
                {
                    this.RaisePropertyChanged();
                }
            }
        }

        [Required]
        public string Producer
        {
            get => this.producer;
            set
            {
                if (SetField(ref this.producer, value))
                {
                    this.RaisePropertyChanged();
                }
            }
        }

        [Required]
        public double Price
        {
            get => this.price;
            set
            {
                if (SetField(ref this.price, value))
                {
                    this.RaisePropertyChanged();
                }
            }
        }
        [Required]
        public int Pegi
        {
            get => this.pegi;
            set
            {
                 if (SetField(ref this.pegi, value))
                 {
                     this.RaisePropertyChanged();
                 }
                
            }
        }

        public int YearofPremiere
        {
            get => this.yearofPremiere;
            set
            {
                if (SetField(ref this.yearofPremiere, value))
                {
                    this.RaisePropertyChanged();
                }
            }
        }

        public string GetCsv(string separator) => string.Join(separator, this.name, this.genre, this.producer, this.price, this.pegi, this.yearofPremiere);

        private static bool SetField<T>(ref T field, T value)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            field = value;
            return true;
        }

        private void RaisePropertyChanged([CallerMemberName]string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

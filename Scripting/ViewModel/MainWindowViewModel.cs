﻿namespace Scripting.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Input;

    using Scripting.Model;

    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string neoLuaText, neoLuaOutput;

        private string moonSharpText, moonSharpOutput;

        public event PropertyChangedEventHandler PropertyChanged;

        public static RoutedCommand CustomRoutedCommandNew { get; } = new RoutedCommand();

        public static RoutedCommand CustomRoutedCommandSave { get; } = new RoutedCommand();

        public static RoutedCommand CustomRoutedCommandDelete { get; } = new RoutedCommand();

        public static RoutedCommand ExecuteNeoLuaRoutedCommand { get; } = new RoutedCommand();

        public static RoutedCommand ExecuteMoonSharpRoutedCommand { get; } = new RoutedCommand();

        public static RoutedCommand ExitRoutedCommand { get; } = new RoutedCommand();

        public ObservableCollection<Entity> Entities { get; } = new ObservableCollection<Entity>();

        public string NeoLuaText
        {
            get => this.neoLuaText;
            set
            {
                this.neoLuaText = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.NeoLuaText)));
            }
        }

        public string NeoLuaOutput
        {
            get => this.neoLuaOutput;
            set
            {
                this.neoLuaOutput = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.NeoLuaOutput)));
            }
        }

        public string MoonSharpText
        {
            get => this.moonSharpText;
            set
            {
                this.moonSharpText = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.MoonSharpText)));
            }
        }

        public string MoonSharpOutput
        {
            get => this.moonSharpOutput;
            set
            {
                this.moonSharpOutput = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.MoonSharpOutput)));
            }
        }

        public string GetSerializedEntities() => string.Join(Environment.NewLine, this.Entities.Select(e => e.GetCsv(";")));

        public void LoadSerializedEntities(string data)
        {
            this.Entities.Clear();
            var entities = from e in data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                           let s = e.Split(';')
                           select new Entity(s[0], s[1], s[2], double.Parse(s[3]), int.Parse(s[4]), int.Parse(s[5]));
            foreach (var entity in entities)
            {
                this.Entities.Add(entity);
            }
        }
    }
}
